# README #

Transforma distintos archivos en el formato .arff para weka.

### ¿Que hace este repositorio? ###

* Lee diferentes archivos(csv,txt) y los transforma en .arff

### ¿Como funciona este repositorio? ###

* El programa lee un archivo y según su formato ejecuta una rutina u otra.
* Al final se produce un archivo .arff perfectamente legible por el programa weka.

### Tecnología ###

* Java
* API de Weka(http://www.cs.waikato.ac.nz/ml/weka/)

### Creador ###

* Jonathan Guijarro Garcia