package conversorArff;

import java.io.*;
import java.util.Scanner;

import weka.core.*;

public class TextoAArff {

	@SuppressWarnings("resource")
	public void createDataset(String directoryPath) throws Exception {

		FastVector atts = new FastVector(2);
		atts.addElement(new Attribute("filename", (FastVector) null));
		atts.addElement(new Attribute("contents", (FastVector) null));
		Instances data = new Instances("text_files_in_" + directoryPath, atts, 0);
		File dir = new File(directoryPath);// le entra el path que dentro estan
											// las dos carpetas neg y pos

		try {
			Scanner entrada = new Scanner(dir);
			String linea;

			while (entrada.hasNext()) {
				double[] newInst = new double[2];
				linea = entrada.nextLine();
				String cabeza = linea.substring(0, 4);
				cabeza = cabeza.replace("\t", "");
				String cuerpo = linea.substring(4, linea.length());
				cuerpo.replace("\t", ",");
				if (!cabeza.equals("ham") && (!cabeza.equals("spam"))) {
					newInst[0] = Instance.missingValue();
				} else {
					newInst[0] = (double) data.attribute(0).addStringValue(cabeza);
				}
				newInst[1] = (double) data.attribute(1).addStringValue(cuerpo);
				data.add(new Instance(1.0, newInst));

			}

		} catch (Exception e) {
			System.out.println("############Error de cargado############");

		}
		escribir(data);
	}

	public static void escribir(Instances inst) {

		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			int z = 0;
			fichero = new FileWriter("sms.arff");
			pw = new PrintWriter(fichero);
			int ultimo = inst.numInstances();
			pw.println("@relation test");
			pw.println("@attribute 'Class' {'ham','spam'}");
			pw.println("@attribute Mensaje string");
			pw.println("@data");
			while (z != ultimo) {
				pw.print(inst.instance(z));
				pw.println();
				z++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fichero)
					System.out.println("Archivo sms.arff creado");
					fichero.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}