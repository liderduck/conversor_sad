package conversorArff;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class CsvAArff {

	public void createDataset(String directoryPath) throws Exception {

		FastVector atts = new FastVector(2);
		atts.addElement(new Attribute("filename", (FastVector) null));
		atts.addElement(new Attribute("contents", (FastVector) null));
		Instances data = new Instances("text_files_in_" + directoryPath, atts, 0);
		File dir = new File(directoryPath);// le entra el path que dentro estan
											// las dos carpetas neg y pos
		
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		String clase="?";
		try {

			br = new BufferedReader(new FileReader(dir));
			line = br.readLine();//necesito saltar la primera linea
			while ((line = br.readLine()) != null) {
				String texto="";
				if(line.contains("Oct")){
					double[] newInst = new double[2];
					String[] frase = line.split(cvsSplitBy);
					clase=frase[1];
					for (int z=0;z<frase.length;z++){
						if (z==1){
							z++;
						}else{
							texto=texto.concat(frase[z]);
						}
					}
					clase=clase.replaceAll("\"", "");
					if (clase.equals("UNKNOWN")){
						newInst[0] = Instance.missingValue();
					}else{
						newInst[0] = (double) data.attribute(0).addStringValue(clase);
					}
					newInst[1] = (double) data.attribute(1).addStringValue(texto);
					data.add(new Instance(1.0, newInst));
					
				}else{
					
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		escribir(data);
		
	}

	public static void escribir(Instances inst) {

		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			int z = 0;
			fichero = new FileWriter("tweeter.arff");
			pw = new PrintWriter(fichero);
			int ultimo = inst.numInstances();
			pw.println("@relation test");
			pw.println("@attribute 'Class' {'negative','neutral','irrelevant','positive'}");
			pw.println("@attribute Mensaje string");
			pw.println("@data");
			while (z != ultimo) {
				pw.print(inst.instance(z));
				pw.println();
				z++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fichero)
					System.out.println("Archivo tweeter.arff creado");
					fichero.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}