package conversorArff;

import java.io.*;
import weka.core.*;

public class TextDirectoryToArff {

	@SuppressWarnings("resource")
	public void createDataset(String directoryPath) throws Exception {

		FastVector atts = new FastVector(2);
		atts.addElement(new Attribute("filename", (FastVector) null));
		atts.addElement(new Attribute("contents", (FastVector) null));
		Instances data = new Instances("text_files_in_" + directoryPath, atts, 0);
		String directoryPathExt;
		String clase;
		String[] filesExt;
		boolean missing = false;
		
		File dir = new File(directoryPath);// le entra el path que dentro estan
											// las dos carpetas neg y pos
		String[] files = dir.list();// listamos ambas carpetas

		for (int q = 0; q < files.length; q++) {
			System.out.println();
			clase = files[q];
			if (!files[q].endsWith(".txt")) {
				File dir1 = new File(directoryPath + "\\" + files[q]);// le
																		// asigamos
																		// la
																		// carpeta
																		// neg
				directoryPathExt = directoryPath + "\\" + files[q];
				filesExt = dir1.list();
			} else {
				
				directoryPathExt = directoryPath;
				filesExt = dir.list();
				missing = true;
			}
			for (int i = 0; i < filesExt.length; i++) {
				try {
					double[] newInst = new double[2];
					if (missing){
					newInst[0]= Instance.missingValue();
					}else{
					newInst[0] = (double) data.attribute(0).addStringValue(clase);
					}
					File txt = new File(directoryPathExt + File.separator + filesExt[i]);
					InputStreamReader is;
					is = new InputStreamReader(new FileInputStream(txt));
					StringBuffer txtStr = new StringBuffer();
					int c;
					while ((c = is.read()) != -1) {
						txtStr.append((char) c);
					}
					newInst[1] = (double) data.attribute(1).addStringValue(txtStr.toString());
					data.add(new Instance(1.0, newInst));
				} catch (Exception e) {
					
				}
			}
			if (missing){
				break;
			}
		}
		escribir(data);
	}
	public static void escribir(Instances inst) {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			int z = 0;
			fichero = new FileWriter("movie.arff");
			pw = new PrintWriter(fichero);
			int ultimo = inst.numInstances();
			pw.println("@relation test");
			pw.println("@attribute 'Class' {'pos','neg'}");
			pw.println("@attribute Mensaje string");
			pw.println("@data");
			while (z != ultimo) {
				pw.print(inst.instance(z));
				pw.println();
				z++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fichero)
					System.out.println("Archivo movie.arff creado");
					fichero.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}