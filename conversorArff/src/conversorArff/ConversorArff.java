package conversorArff;

public class ConversorArff {
	public static void main(String[] args) throws Exception {
			
		if(args[0].endsWith(".txt")){
			System.out.println("Prerando conversion de txt a arff");
			TextoAArff txtArff = new TextoAArff();
			txtArff.createDataset(args[0]);
		}else if (args[0].endsWith(".csv")){
			System.out.println("Prerando conversion de csv a arff");
			CsvAArff csvArff = new CsvAArff();
			csvArff.createDataset(args[0]);
			
		}else{
			System.out.println("Prerando la lectura de carpetas para crear el arff");
			TextDirectoryToArff fileArff = new TextDirectoryToArff();
			fileArff.createDataset(args[0]);
			
		}
	}
}
